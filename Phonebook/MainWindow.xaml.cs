﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Phonebook.Logic;


namespace Phonebook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            System.Diagnostics.Debug.WriteLine("AppStarted");

            // use object oriented get and set
            Contact contact1 = new Contact();
            contact1.FirstName = "Cheatra";
            contact1.LastName = "Soeung";
            contact1.Gender = Gender.Male;
            contact1.DOB = new DateTime(1992, 1, 1);
            System.Diagnostics.Debug.WriteLine("Display Name: " + contact1.GetDisplayName());
            System.Diagnostics.Debug.WriteLine(contact1.PrintAllInfo());

            // always passing through parameter
            Contact contact2 = new Contact();
            System.Diagnostics.Debug.WriteLine("Display Name: " + contact2.GetDisplayName("Pengwen", "Wong"));
            System.Diagnostics.Debug.WriteLine(contact2.PrintAllInfo("Pengwen", "Wong", Gender.Male, new DateTime(1982, 9, 23)));

            //define array
            Contact[] collection = new Contact[2];
            collection[0] = contact1;
            collection[1] = contact2;

            System.Diagnostics.Debug.WriteLine("Contact2 FirstName:" + contact2.FirstName);

            //method 1 to call array
            for (int i = 0; i < collection.Length; i++)
            {
                System.Diagnostics.Debug.WriteLine(collection[i].FirstName);
            }

            //method 2 to call array
            foreach (Contact row in collection)
                System.Diagnostics.Debug.WriteLine(row.FirstName);

            //start bind into GUI
            Textblock1.Text = contact1.FirstName; //call function in xaml file
            Textblock2.Text = contact1.LastName;

        }
    }
}