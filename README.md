# Dotnet Core
Learn to build dotnet core application cross platforms.

## Pre-requisition
- [Download .NET SDK (64-bit)](https://download.visualstudio.microsoft.com/download/pr/8a382be6-d34e-4a15-92a1-e49fe2fa54d6/0038e8d5447470750098f8db5d836561/dotnet-sdk-3.0.100-win-gs-x64.exe) for build and compile app
- [Install .NET Core Runtime in specified OS](https://dotnet.microsoft.com/download/) for running the app
- ``sudo snap install dotnet-sdk --classic`` using snap in Ubuntu.

## Getting Started
```
> dotnet new console -o myApp
> cd myApp
> dotnet run
```

```
> dotnet new wpf
> dotnet run
```

```
> dotnet new winforms
> dotnet run
```

```
> dotnet new classlib
> dotnet new xunit
```

Create an ASP.NET MVC Website

```
dotnet new mvc
```

## Publish
```
> dotnet publish -c release -r ubuntu.18.04-x64
```

## Launch App in Ubuntu/Linux
```
$ ./appname
```
** no extension
