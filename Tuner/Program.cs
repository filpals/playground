﻿using System;

namespace Tuner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Optimizer optimizer = new Optimizer();
            optimizer.OptimizeGolden();
            optimizer.OptimizeActual();

            Console.ReadKey();
        }
    }
}
