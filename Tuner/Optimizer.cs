﻿namespace Tuner
{
    /// <summary>
    /// Concrete class (implementation class).
    /// </summary>
    public class Optimizer
    {
        private OptimizerBase solver;
        public Optimizer()
        {

        }

        public void OptimizeGolden()
        {
            solver = new Nlopt();
            solver.Generate();
            solver.Optimize();
        }
        public void OptimizeActual()
        {
            solver = new AlgLib();
            solver.Generate();
            solver.Optimize();
        }
    }
}