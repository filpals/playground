﻿using System;

namespace Tuner
{
    public class Nlopt : OptimizerBase
    {
        public override void Generate()
        {
            Console.WriteLine("Nlopt.Generate()");
        }

        public override bool Optimize()
        {
            Console.WriteLine("Nlopt.Optimize()");
            return false;
        }
    }
}
