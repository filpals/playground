﻿using System;

namespace Tuner
{
    public class AlgLib : OptimizerBase
    {
        public override void Generate()
        {
            Console.WriteLine("AlgLib.Generate()");
        }

        public override bool Optimize()
        {
            Console.WriteLine("AlgLib.Optimize()");
            return false;
        }
    }
}