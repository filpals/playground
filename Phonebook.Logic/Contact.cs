﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phonebook.Logic
{
    public class Contact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public Gender Gender { get; set; }
        public Contact() { }
        /// <summary>
        /// Return display name in format of FirstName, LastName.
        /// </summary>
        /// <returns></returns>
        public string GetDisplayName()
        {
            return this.FirstName + ", " + this.LastName;
        }
        /// <summary>
        /// Return display name in format of FirstName, LastName.
        /// </summary>
        /// <returns></returns>
        public string GetDisplayName(string first, string last)
        {
            return first + ", " + last;
        }
        public string PrintAllInfo()
        {
            return string.Format("{0}, {1}({2}, {3})", this.FirstName, this.LastName, this.Gender, this.DOB.ToShortDateString());
        }
        public string PrintAllInfo(string first, string last, Gender gender, DateTime dob)
        {
            // see https://msdn.microsoft.com/en-us/library/system.string.format(v=vs.110).aspx
            return string.Format("{0}, {1}({2}, {3})", first, last, gender, dob.ToShortDateString());
        }
    }
}