﻿namespace Tuner
{
    /// <summary>
    /// Base class for optimizer (implement abstract factory pattern).
    /// </summary>
    public abstract class OptimizerBase
    {
        public abstract void Generate();
        public abstract bool Optimize();
    }
}
